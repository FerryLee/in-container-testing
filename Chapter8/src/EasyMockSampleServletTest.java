import static org.junit.Assert.*;

import javax.servlet.http.*;

import org.easymock.EasyMock;
import org.junit.*;

import static org.easymock.EasyMock.*;

public class EasyMockSampleServletTest {

	private SampleServer servlet;
	private HttpServletRequest mockHttpServletRequest;
	private HttpSession mockHttpSession; 
	
	@Before
	public void setUp() throws Exception {
		servlet=new SampleServer();
		mockHttpServletRequest=EasyMock.createMock(HttpServletRequest.class);
		mockHttpSession=EasyMock.createMock(HttpSession.class);
	}



	@Test
	public void testIsAuthenticatedAuthenticated() {
		EasyMock.expect(mockHttpServletRequest.getSession(eq(false)))
		.andReturn(mockHttpSession);
		EasyMock.expect(mockHttpSession.getAttribute(eq("authenticated"))).andReturn("true");
		replay(mockHttpServletRequest);
		replay(mockHttpSession);
		assertTrue(servlet.isAuthenticated(mockHttpServletRequest));
	}

	@Test
	public void testIsAuthenticatedNotAuthenticated(){
		expect(mockHttpSession.getAttribute(eq("authenticated"))).andReturn("false");
		replay(mockHttpSession);
		expect(mockHttpServletRequest.getSession(false)).andReturn(mockHttpSession);
		replay(mockHttpServletRequest);
		assertFalse(servlet.isAuthenticated(mockHttpServletRequest));
	}
	
	@Test
	public void testIsAuthenticatedNoSession(){
		expect(mockHttpServletRequest.getSession(eq(false))).andReturn(null);
		replay(mockHttpServletRequest);
		replay(mockHttpSession);
		assertFalse(servlet.isAuthenticated(mockHttpServletRequest));
	}
	
	@After
	public void tearDown() throws Exception {
		verify(mockHttpServletRequest);
		verify(mockHttpSession);
	}
}



































